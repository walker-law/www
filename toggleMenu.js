function toggleMenu() {
    var menu = document.getElementById('nav-menu');
    var display = getComputedStyle(menu).display;

    menu.style.display = display === 'none' ? 'flex' : 'none';
}
